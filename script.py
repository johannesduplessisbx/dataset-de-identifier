from sys import argv
from array import array
import os
import shutil
import hashlib
import json
import pandas as pd


# Check if correct arguments were given
if len(argv) != 3:
        print("Expected one imagepath argument and one destination path argument:\n"
                   "    $ python script.py path/to/files path/to/destination")
        exit(0)

# Specifying arguments
filepath = argv[1]
outdir = argv[2]


data = {}
for i, filename in enumerate(os.listdir(filepath)):
        old_file = os.path.join(filepath, filename)
        # Separate base from extension
        base, extension = os.path.splitext(filename)
        baselist = base.split("_")
        old_name = baselist[0]
        gender = baselist[1]
        ethnicity = baselist[2]
        age_range = baselist[3]
        new_name = int(hashlib.sha1(old_name.encode("utf-8")).hexdigest(), 16) % (10 ** 8)
        new_file = os.path.join(outdir, str(new_name) + "_1.jpg")
        shutil.copy(old_file, new_file)
        data[new_name] = [old_name, gender, ethnicity, age_range]
df = pd.DataFrame.from_dict(data, orient='index')
print(df)
df.to_csv(outdir + "/map.csv")